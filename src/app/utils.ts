export const getFileBuffer = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      resolve(e.target.result);
    };
    reader.onerror = (e: any) => {
      reject(e.target.error);
    };
    reader.readAsArrayBuffer(file);
  });
};
