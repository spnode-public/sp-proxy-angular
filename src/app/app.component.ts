/// <reference path="./../../node_modules/@types/sharepoint/index.d.ts" />

import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { getFileBuffer } from './utils';

const proxyUrl = 'http://localhost:8080';
const docLibTitle = 'Documents';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public title = '...';
  private webRelativeUrl: string;
  private requestDigest: string;

  constructor (private http: HttpClient) {
    if (window.location.href.indexOf('http://localhost') === 0) {
      this.webRelativeUrl = proxyUrl;
      this.requestDigest = 'dummy';
    } else {
      this.webRelativeUrl = _spPageContextInfo.webServerRelativeUrl;
      this.requestDigest = (document.getElementById('__REQUESTDIGEST') as any).value;
    }
    this.loadSiteTitle();
  }

  public uploadDocument(event) {
    console.log('Upload a Document');
    const file = event.srcElement.files[0];
    console.log(file);
    getFileBuffer(file)
      .then(buffer => {
        const uploadUrl = `${this.webRelativeUrl}/_api/web/lists/` +
          `getbytitle('${docLibTitle}')/rootfolder/files/` +
          `add(url='${file.name}',overwrite='true')`;
        return this.http.post(uploadUrl, buffer, {
          headers: new HttpHeaders()
            .append('Accept', 'application/json;odata=verbose')
            // .append('Content-Type', 'application/json;odata=verbose')
            .append('X-RequestDigest', this.requestDigest)
            .append('processData', 'false')
        }).toPromise();
      })
      .then(console.log)
      .catch(console.log);
  }

  private loadSiteTitle () {
    this.http.get(`${this.webRelativeUrl}/_api/web?$select=Title`, {
      headers: new HttpHeaders()
        .append('Accept', 'application/json;odata=verbose')
    })
    .toPromise()
    .then((res: any) => {
      this.title = res.d.Title;
    });
  }

}
